# The module is designed to clean up pictures that are no longer in use by crown;)

###### Settings ````settings.php````

````
$config['clean_files_entity'] = [
	'folders'		=> [
		'public://node_images/',
	],
	'max'			=> 100
];
````